# __Dopełniacz__ (_birtokos eset_) - <sup>Kogo?</sup> <sub>Czego?</sub>

## Végződések

![Birtokos eset végződései](../media/img/birtokos_vegzodesek.png)

## Birtokos esettel kifejezendő esetek :thinking:

### A.) Birtokviszony

A legalapvetőbb használata ennek az esetnek az, amikor egyszerűen birtokviszonyt akarunk kifejezni. Ellentétben a magyarral, ami a birtokolt dolgot ragozza (_Az anyukám haja szép._), a lengyel a birtoklót ragozza és a dolgot pedig békénhagyja (__"haj anyukámé"__). Az is jellemző, hogy a birtokolt dolog a birtokló előtt szerepel.

- Włosy __mojej matki__ są ładne. - _Anyukám haja szép._
- Czytam książkę __mojego ojca__. - _Az apukám könyvét olvasom._
- Lubię dobrego psa __mojego kolegi__. - _Szeretem a haverom jó kutyáját._

### B.) Prepozíciók

Egyes (_rengeteg_) elöljárószók birtokos esettel járnak:

- __do + dop.__: -ba, -be, -ra, -re, -ig.. "oda" (__hely és idő__)!
  - Jedziemy __do Krakowa__! - _Krakkóba megyünk!_
  - Jutro mam zajęcia __do piętnastej__. - _Holnap dolgom (ti. elfoglaltságom) van háromig._
  - Jadę pociągiem z Budapesztu __do Debreczyna__. - _Vonattal megyek Budapestről Debrecenbe._

- __od + dop.__: -ból, -ról.. "onnan" (__hely és idő__)!
  - Jutro mam zajęcia __od piętnastej__. - _Holnap dolgom (ti. elfoglaltságom) van háromtól._

- __bez + dop.__: nélkül (pl.: cukor nélkül: _bez cukru_)
  - Dziękuję, ale piję herbatę __bez cukru__. - _Köszönöm, de cukor nélkül iszom a teát._
  - Chodźmy __bez Martyny__, bo ona jeszcze nie jest gotowa! - _Menjünk Martina nélkül, mert ő még nincs kész!_

- __dla + dop.__: -nak, -nek, számára, részére.. (gyerekeknek: _dla dzieci_)
  - Książki __dla dzieci__. - _Könyvek gyerekeknek._

- __z + dop.__: -ból, -ből (a boltból: _ze sklepu_, málnából: _z malin_)
  - Jadę pociągiem __z Budapesztu__ do Debreczyna. - _Vonattal megyek Budapestről Debrecenbe._
  - __Życzę wszystkiego najlepszego z okazji twoich urodzin__! - _Minden jót (ti. legjobbat) kívánok a szülinapod alkalmából._

- __u + dop.__: -nál, -nél (__szigorúan csak személy__)!
  - Byłem rano __u lekarza..__ - _Voltam reggel orvosnál.._
  - Krzysztof, widziałeś gdzieś papiery __księgowego__? - _Kristóf, láttad valahol a könyvelő papírjait?_

- __obok + dop.__: közvetlenül valami mellett
  - Krzesło jest obok __stołu__. - _A szék az asztal mellett van._
  - __Obok żółtego tramwaju__ jedzie zielony samochód. - _A sárga villamos mellett megy a zöld autó._

- __koło + dop.__: valami mellett (környékén..)
  - __Koło Krakowa__ jest Olkusz. - _Krakkó mellett (ti. a környékén) van Olkus._
  - To miasto nazywa się Kołobrzeg, bo jest __koło brzegu Bałtyku__. - _Ezt a várost Kołobrzegnek hívják (ti. nevezi magát), mert a Balti-tenger partja mellett van._

- __około + dop.__: valami körül (környékén..)
  - Bardzo ładna droga jest __około śródmiejścia Krakowa__. - _Nagyon szép út van Krakkó belvárosa körül._
  - __Około ósmej__ zacznie się program. - _Nyolc (ti. óra) körül kezdődik el a program._

- __(nie)daleko + dop. + (od + dop.)__: (nem)messze valamitől
  - Ach Damian, to miejsce jest bardzo __daleko__! - _Ah Damian, az a hely nagyon messze van._

- __blisko + dop.__: közel valamihez
  - Ja mieszkam __blisko szkoły__, ale Paweł nie. - _Én közel lakom az egyetemhez, de Pali nem._
  - Codziennie rano szedłem do piekarni, która jest __blisko mojego mieszkania__. - ~_Minden nap reggel abba a pékségbe mentem, amelyik a lakásomhoz van közel._

- __na środku + dop.__: valaminek a közepén
  - AA, Kasia, pająk jest __na środku twoich pleców__! - _ÁÁ, Kati, a hátad közepén van egy pók!_

- __naprzeciwko + dop.__: szemközt, valamivel szemben
  - Sklep jest __naprzeciwko poczty__. - _A bolt a postával szemben van._

- __podczas + dop.__: alatt (idő)
  - Zwiedzam miasto __podczas pobytu__ we Francji. - _Várost nézek a Franciaországi tartózkodásom alatt._

- __oprócz + dop.__: valamin kívül
  - Wszystko mam __oprócz aparatu fotograficznego__.. - _Mindenem megvan a fényképezőn kívül.._

- __zamiast + dop.__: valami helyett
  - Kup kamerę video __zamiast aparatu__! - _Vegyél videokamerát a fényképező helyett!_

- __według + dop.__: valami szerint
  - Wiesz co? __Według mnie__ jesteś głupi. - _Tudod mit? Szerintem hülye vagy._

- __dookoła + dop.__: körül (körbe-körbe  teljes kör)
  - Bieganie __dookoła jeziora__. - _Futás a tó körül._

- __zza + dop.__: vmi/vki mögül
  - Samochód wyjechał __zza domu__. - _Az autó elment a ház mögül._

- __spod + dop.__: vmi/vki alól
  - Pies wyszedł __spod stołu__. - _A kutya elment az asztal alól._

- __znad + dop.__: vmi/vki felől (hely)
  - Wiatr wieje __znad morza__. - _A szél a tenger felől fúj._
  - Boże, żołnierze przychodzą __znad granicy__! - _Istenem, katonák jönnek a határ felől!_

- __sprzed + dop.__: vmi/vki elől, "előttről"
  - Pies ukradł jej burgera __sprzed nosa__. - _A kutya ellopta a burgerét az orra elől (ti. az orr elől)._

- __spomiędzy + dop.__: vmik/vkik közül
  - Wyszliśmy __spomiędzy drzew__. - _Kimentünk a fák közül._

- __wśród + dop.__: vmik/vkik között
  - Dom stał __wśród drzew__. - _Ház állt az fák közt._

Az [__-e__] betűnek, hangnak kiejtéskönnyítő szerepe is van. Például a [__z__] prepozíciót gyakran magyar [__sz__]-nek ejtik, ezért nehéz lenne kimondani, hogy a _boltból_ (~~z sklepu~~ helyett __ze sklepu__).

### C.) Igevonzatok

Vannak olyan igék, amelyek minden "normális" nyelvben tárgyesettel járnak, de persze a lengyelben nem illetve egyéb igék, melyek birtokos esettel járnak. Idáig ezekkel foglalkoztunk:

- __słuchać + dop.__: hallgatni vmit
  - Słucham __muzyki__. - _Zenét hallgatok._
  - Słuchasz __tego głosu__? - _Hallod ezt a hangot?_

- __szukać + dop.__: keresni vmit
  - __Szukam noclegu__ w Budapescie. - _Szállást keresek Budapesten._
  - __Szukałem chomika__ pod łóżkiem także, ale nie był tam. - _A hörcsögöt az ágy alatt is kerestem, de nem volt ott._

- __chcieć + dop./bier.__: akarni vmit
  - __Chcę spokoju!__ - _Nyugalmat akarok!_
  - Chcę wodę i chleb. - _Kenyeret és vizet akarok._

- __życzyć + dop.__: kívánni vmit
  - Matu, __życzę dobrej pracy__. - _Máté, jó munkát kívánok._
  - __Życzę wszystkiego najlepszego z okazji twoich urodzin__! - _Minden jót (ti. legjobbat) kívánok a szülinapod alkalmából._

- __spodziewać się + dop.__: várni/remélni vmit (ami be fog következni, "to expect")
  - __Spodziewam się gości__. - _Várom a vendégeket._
  - Moja żona __spodziewa się dziecka__. - _A feleségem gyereket vár._

- __potrzebować + dop.__: szükségnek lenni valamire (__Potrzebuję spokoju__)
  - __Potrzebuję twojej opinii__. - _Szükségem van a véleményedre._

- __używać + dop.__: használni (valamit)
  - Przepraszam, ale muszę __uzywać toalety__! - _Bocsánat, de használnom kell a wc-t!_

_Tipp_: Élő nyelvben a [__chieć__]-t birtokos esettel használják, ha elvont dologról beszélnek (pl. érzelemből) vagy valami dolgot már nagyon nagyon akarnak, indulatból, illetve tárgyesettel, ha egyéb, megfogható dolgot szeretnének.

- Chcę jeden piwo (_bier._). - _Egy sört akarok._
- Chcę __spokoju__ (_dop._)! - _Nyugalmat akarok!_

### D.) Tagadás

Tárgyesetet vonzó igék tagadásakor birtokos esetet kell használni. Ez azt jelenti, hogy az _olvasni valamit, nézni valamit, stb.._ példákban a __valami__ tagadáskor birtokos esetben kell szerepeljen. Az olyan __igék__ esetében, amelyek __nem tárgyesetet vonzanak__, ez a szabály __nem érvényes__.

- Ja bardzo lubię __kawę__! → Ja bardzo nie lubię __kawy__!
  - _Én nagyon szeretem a kávét!_ → _Én nagyon nem szeretem a kávét!_

- Oglądam __telewizor__. → Nie oglądam __telewizora__.
  - _Tévét nézek._ → _Nem nézek tévét._

- Uźywam __długopis__. → Nie używam __długopisu__.
  - _Tollat használok._ → _Nem használok tollat._

- Czytam __tą ciekawą ksiaźkę__. → Nie czytam (jeszcze) __tej ciekawej książki__.
  - _Olvasom ezt az érdekes könyvet._ → _Nem olvasom (még) ezt az érdekes könyvet._

- Uwielbiam __tego ślicznego psa__! → Nie lubię __tego ślicznego psa__.
  - _Imádom ezt az aranyos/gyönyörű kutyát._ → _Nem szeretem ezt az aranyos kutyát._

- Czytam __książkę__ mojego ojca. → Nie czytam __książki__ mojego ojca.
  - _Apukám könyvét olvasom._ → _Nem olvasom apukám könyvét._

- Oglądam __ten ciekawy program telewizyjny__. → Nie oglądam __tego nudnego programu telewizyjnego__.
  - _Nézem ezt az érdekes tvműsort._ → _Nem nézem ezt az unalmas tvműsort._

- Potrzebuję __spokoju__ (_potrzebuje + dop._). → Nie potrzebuję __spokoju__.
  - _Nyugalomra van szükségem._ → _Nincs szükségem nyugalomra._

- Interesuję się __ogrodami__. → Nie interesuję się __ogrodami__.
  - _Érdeklődöm a kertek iránt._ → _Nem érdeklődöm a kertek iránt._

Ezekben a példákban több kivétel is volt. Van amire van recept, de arra, hogy pl. miért nem számít a toll eszköznek nincs, ezeket sajnos meg kell tanulni. Azonban nem csak ilyenkor kell birtokos esetet használni. A tagadás úgy általában ezt az esetet vonzza. Ha bármilyen tagadószó, például..

- __bez__
- __nie__
- __ani .. ani ..__

..megjelenik egy mondatban, akkor azonnal birtokos esetbe kerül a dolog, amire vonatkozik.

- Piję herbatę z cukrem. - _Cukorral iszom a teát._
  - __Nie__ piję __herbarty__ z cukrem. - _Nem iszom cukorral a teát._
  - Piję herbartę __bez cukru__. - _Cukor nélkül iszom a teát._
  - __Nie__ piję __ani herbaty ani kawy bez cukru__! - _Nem iszom sem a teát sem a kávét cukorral._

- __Nie__ lubię __kwaśnego smaku cytryny__. - _Nem szeretem a citrom savanyú ízét._

### E.) Mennyiségek

A határozatlan mennyiségjelzőkkel - például..

- __dużo__
- __Ile?__
- stb..

..birtokos esetet kell használni, azonban nem mindegy, hogy egyes vagy többesszámot. __Egyes számot__ akkor használunk, ha __nem megszámlálható__ dologról beszélünk.

- Potrzebuję dużo __spokoju__.
- Pijesz dużo __Coli__!!

__Többes számot__ akkor használunk, ha __megszámlálható__ dologról beszélünk.

- Kupiłem dużo __opakowań__ jajek.
- Zjadłem dużo __kawałków__ pizzy.
- Wpiłem dużo __szklanek__ wody.

Számok esetén alanyeset és birtokos eset váltakozik a következőképpen.

1. Ha a dolog __egy darab__, akkor értelemszerűen __alanyeset egyes számot__ használunk.
2. Ha a dolog __2, 3, 4__ illetve olyan nem összetett szám, ami __ezekre végződik__ (_pl. 1403_), akkor __alanyeset többes számot__ használunk.
3. Az összes többi esetben birtokos esetet használunk. Többes számot, ha az adott dolog megszámlálható, illetve egyes számot, ha nem.

Egyszerűsítve: __Ha 4< birtokos eset, kivétel, ha a szám 2, 3, 4-re végződik__.

Nézzük most meg ezt példával.

1.) X kiló eper: A kiló (meg úgy az összes mértékegység..) is és az eper is megszámlálható.. "darabos"..

- 1 kilogram (_alanyeset egyes szám_) truskaw __-ek__ (_birtokos többes szám_)
- 2 kilogram __-y__(_alanyeset többes szám_) truskaw __-ek__ (_birtokos többes szám_)
- 3 kilogram __-y__(_alanyeset többes szám_) truskaw __-ek__ (_birtokos többes szám_)
- 4 kilogram __-y__(_alanyeset többes szám_) truskaw __-ek__ (_birtokos többes szám_)
- 5 kilogram __-ów__ (_birtokos többes szám_) truskaw __-ek__ (_birtokos többes szám_)
- 13 (_trzynaście_) kilogram __-ów__ (_birtokos többes szám_) truskaw __-ek__ (_birtokos többes szám_)
- 23 (_dwadzieścia trzy(!)_) kilogram __-y__ (_alanyeset többes szám_) truskaw __-ek__ (_birtokos többes szám_)

2.) X üveg tej: A tej, mint folyadék pedig nem..

- 1 butelka (_alanyeset egyes szám_) mlek __-a__ (_birtokos egyes szám_)
- 2 butelk __-i__(_alanyeset többes szám_) mlek __-a__ (_birtokos egyes szám_)
- 3 butelk __-i__(_alanyeset többes szám_) mlek __-a__ (_birtokos egyes szám_)
- 4 butelk __-i__(_alanyeset többes szám_) mlek __-a__ (_birtokos egyes szám_)
- 5 butel __-ek__ (_birtokos többes szám_) mlek __-a__ (_birtokos egyes szám_)
- 13 butel __-ek__ (_birtokos többes szám_) mlek __-a__ (_birtokos egyes szám_)
- 23 butelk __-i__ (_alanyeset többes szám_) mlek __-a__ (_birtokos egyes szám_)

### F.) Időhatározók

Korábban már láttuk, hogy [eszközhatározó esettel](eszkozhatarozo_eset-narzednik.md#c-id%C5%91hat%C3%A1roz%C3%B3k) ki tudunk fejezni időbeli dolgokat. A birtokos esetnek van egy ilyen jelentése is!

- tamten dzień (_az a nap_) → __tamtego dnia__ (_azon a napon_)
  - __Tamtego dnia__ nie zjadłem śniadania. - _Azon a napon nem ettem reggelit._
  - Było bardzo ciepło __tego lata__. - _Nagyon meleg volt ezen a nyáron._

## A szabálytól való eltérések (biztosan [__-a__])

A sok kivétel közt eligazodni segít egy pár szituációra jól alkalmazható recept.

### Lengyel városok

Minden lengyel város [__-a__] végződést kap!

- Kraków → Krakowa
- Poznań → Poznania
- stb..

Ezeken kívül két nem lengyel város kap [__-a__]-t:

- Berlin → ~~Berlinu~~ Berlina
- Paryż → ~~Paryżu~~ Paryża

### Hónapok

Ugyan a hónapok nevei mind hímneműek, de mégis [__-a__]-t kapnak. A Február (_Luty_) melléknévi eredetű, ezért az egyes számú, hímnemű végződést [__-ego__] fogja kapni. Például:

- Styczeń (_Jan._) → 1 Stycznia
- Luty (_Febr._) → 1 Lutego
- Marzec (_Márc._) → 1 Marca
- stb..

### Testrészek

Azok a testrészek, amelyek hímneműek, szintén [__-a__] végződést kapnak.

- żołądek (_gyomor_) → ból żołądka (_gyomor fájdalma (gyomorfájás..)_)
- palec (_ujj_) → ból palca (_ujj..fájás_)
- brzuch (_has_) → ból brzucha (_hasfájás_)
- stb..

### Eszközök

Azok a dolgok, amikre ráfogható, hogy eszköz..

- komputer (_..._) → komputera
- nóż (_kés_) → noża
- widelec (_villa_) → widelca
- talerz (_tányér_) → talerza
- odkurzacz (_porszívó_) → odkurzacza
- stb..

### Kicsinyített szavak

A kicsinyített szavak szintén [__-a__] végződést kapnak, hiába nem indokolja ezt semmi. Kicsinyítő képzők: [__-ak__], [__-ek__], [__-ik__]

- koperek (_kapor_) → bez koperka
- koperek (_kapor_) → koperka
- kapuśniak → smak kapuśniaka

A példákból azt hihetnénk, hogy ez csak ételekre vonatkozik. __Pedig nem.__

- dziennik (_napilap!_) → strona dziennika (_a napilap oldala_)
- stb..

### Márkanevek, tulajdonnevek

Az autó márkanevek..

- Fiat → kolor Fiata
- Opel → Opla
- Trabant → Trabanta
- stb..

## A szabálytól való eltérések (biztosan [__-u__])

### Megszámlálható gyűjtőnevek

- piasek (_homok_) → piasku (hiába [__-ek__]-re végződik!)
- tłum (_tömeg_) → tłumu
- stb..
