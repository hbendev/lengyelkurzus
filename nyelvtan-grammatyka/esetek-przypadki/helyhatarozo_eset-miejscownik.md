# __Miejscownik__ (_helyhatározó eset_) - <sup>Kim?</sup> <sub>Czym?</sub>

## Végződések

![Eszközhatározó eset végződései](../media/img/helyhatarozo_vegzodesek.png)

## Többes szám képzése

Ez az egyszerűbb, mert itt semmilyen tőváltozás nem történik. Az eddig megszokotthoz hasonlóan a szavak egyszerű ragokat kapnak. A főnevek [__-ach__], a melléknevek [__-ych/ich__] végződéssel lesznek hosszabbak.

Példák főnevekre:

- __góra__ (hegy) → _po górach_
- __kot__ (macska) → _o kotach_
- __stół__ (asztal) → _na stołach_
- __dziennikarz__ (újságíró) → _w dziennikarzach_

Nagyon kevés kivétel van, ezek:

- __Niemcy__ (Németország) → __w__ ~~Niemcach~~ __Niemczech__
- __Węgry__ (Magyarország) → __na__ ~~Węgrach~~ __Węgrzech__
- __Włochy__ (Olaszország) → __we__ ~~Włochach~~ __Włoszech__

Példák melléknevekre (ugyanaz, [mint birtokos](birtokos_eset-dopelniacz.md) esetben):

- __ładny__ (szép) → _o ładnych_
- __mocny__ (erős) → _o mocnych_
- __brzydki__ (csúnya) → _o brzydkich_
- __gorzki__ (keserű) → _o gorzkich_

## Egyes szám képzése

Az egyes szám képzésében az egyes számú ragokon kívül tőváltozások (az utolsó hang/betű megváltozása) is szerepet játszanak. Erre szintén van szabály, amit nagyon be kell gyakorolni.

![Tőváltozások](../media/img/helyhatarozo_vegzodesek_tablazat.png)

Ettől eltérően kevés kivétel van, ezek:

- __syn__ (fia ..valakinek) → ~~synie~~ __synu__
- __pan__ (úr) → ~~panie~~ __panu__
- __dom__ (ház, otthon) → ~~domie~~ __domu__
- __wieś__ (_f_) (falu) → ~~wieśi~~ __wsi__

__FONTOS:__ Az [__-um__]-ra végződő főneveket (pl. muzeum, liceum, stb.) a lengyel nyelv egyáltalán nem ragozza. Azaz:

- __muzeum__ (...) → ~~muzeumie~~ __w muzeum__
- __liceum__ (gimnázium) → ~~liceumie~~ __w liceum__
- stb..

__FONTOS:__ Az idegen eredetű nőnemű szavak, ha [__i__]-re vagy lágy mássalhangzóra (pl.: __ń__) végződnek akkor dupla [__i__] lesz a végén. Ha nem idegen eredetű, akkor nem teszi hozzá a plusz i-t. Azaz:

- __Austria__ → __w Austrii__
- __ekonomia__ → __o ekonomii__
- __Monia__ → __o Moni__
- __Kasia__ → __o Kasi__

## Helyhatározó esettel kifejezendő esetek :thinking:

### A.) Prepozíciók

- __w + miej.__: -ban, -ben
  - Halo Anna? Jesteście __w sklepie__? - _Halló Anna? A boltban vagytok?_
  - Z tatą będziemy __w szkole muzycznej__ w piątek. - _Apával a zeneiskolában leszünk pénteken._

- __na + miej.__: -on, -en, -ön, -in... stb.
  - Mama jest __na wycieczce__. - _Anya (egy) kiránduláson van._
  - O kurwa! Pies jest __na stole__!! - _Ó a kurva életbe! A kutya az asztalon van!!_

- __o + miej.__: -ról, -ről (nem hely); -kor (idő)
  - Agata: Bartku, co myślisz __o tamtym nowym facecie__? - _Agata: Bartek, mit gondolsz arról az új srácról?_
  - Bartek: __O którym__? __O Grzegorzu__? Jest bardzo zdolny! - _Bartek: Kiről? Gergőről? Nagyon ügyes!_
  - Boże, jest tak póżno?! __O ósmej__ mam randkę! - _Istenem, már ilyen késő van?! Nyolckor randim van!_

- __po + miej.__: ... után (időben); ... szerte (hely)
  - __Po wojnie__ otworzyłem kwiaciarnię. - _A háború után virágboltot nyitottam._
  - Codziennie chodzimy na spacer z moim psem nawet __po dziesiątej__. - _Minden nap sétára megyünk a kutyámmal még 10 után is._

- __przy + miej.__: -nál, -nél (__nem ember!__)
  - Daj mi mój telefon proszę. Tam jest __przy moim komputerze__ na stole.  - _Add ide a telefonom kérlek (ti. add nekem). Ott van a gépemnél az asztalon._
  - TATO! Jest pluskwiak w pokoju! Jest __przy oknie__! - _APA! Egy büdösbogár van a szobában! Az ablaknál van!_
  - Proszę uważać __przy stołach__! - _Kérem ügyelni az asztaloknál!_
