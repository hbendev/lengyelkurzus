# __Mianownik__  (_alanyeset_) - <sup>Kto?</sup> <sub>Co?!</sub>

## Birtokos esettel kifejezendő esetek :thinking:

A __kto?__ (ki?) és a __co?__ (mi?) kérdéseket lehet az eset használatához kötni.
Az elnevezéséből (_mianownik = nevező (matematikából is ismert)_) már lehet következtetni a használat eseteire. A következő helyzetekben használjuk:

### A.) A szó a mondat alanya:
- __Jan__ kocha Marię. - _János szereti Máriát._
- __Kot__ pije. - _A macska iszik._

### B.) Megnevezünk valamit/valakit:
- Jestem __Adam Kowalski__. - _Kovács Ádám vagyok._
- To są __moje okulary__. - _Ez az én szemüvegem._

## Végződések

Mivel a lengyel egy indo-európai nyelv, benne is jelen vannak a nyelvtani nemek. Ennél a nyelvnél könnyebb dolgunk van, mint például a németnél, mert a szóvégek elárulják, hogy az adott szó melyik nembe tartozik. Az alábbi diagram ennek a rendszernek az átlátására szolgál, ezek nem kifejezetten "megtanulandó" végződések.

![Az alanyeset végződései](../media/img/alanyeset_vegzodesek.png)

Látjuk, hogy a hímnemű főnevek mássalhangzóra, a nőneműek [__-a__] hangra, néha [__-i__]-re, a semleges neműek pedig arra a sokfélére végződnek.

## Mit érdemes tudni az alanyesetről?

A teljesség igénye nélkül:

- A főneveket, mellékneveket (_jelző_) és számneveket __nemben és esetben mindig egyeztetni__ kell.
- __Állandó kifejezésekben__ - pl. _muzyka klasyczna_ - a jelző a jelzett szó __mögött__ áll (latin hatása).
    - Megjegyzés: _Ha a melléknevet a főnév előtt használjuk, akkor másképp érzik a mondanivalót. A milyenségen van a hangsúly és az eredeti jelentés el is veszhet. Például a "klasyczna muzyka" az inkább olyan klasszikusos.. zene. De nem "klasszikus zene"._
- Az idegen eredetű szavakat általában nőnemű végződéssel veszi át a nyelv.

A szabálytól a következő esetekben van eltérés (_nem teljes lista_):

- Férfi foglalkozások mind hímneműek, még akkor is, ha [__-a__]-ra végződnek! Ha áll mellette melléknév, akkor az természetesen a hímnemű alakjában szerepel.
- Az [__-ość__] végű szavak nőneműek!

## Személyes névmások

|   Lengyel|Magyar|Lengyel|Magyar|
|---------:|:-----|------:|:-----|
|        Ja|Én    |     My|Mi    |
|        Ty|Te    |     Wy|Ti    |
|On/ona/ono|Ő     |Oni/one|Ők    |

## Egyes szám

### Néhány hímnemű példa

- __dobry pies__ (_jó kutya_)
- __znany malarz__ (_ismert festő_)
- __zły policjant__ (_mérges rendőr_)
- __ładny koń__ (_szép ló_)
- __duży stół__ (_nagy asztal_)
- __wysoki chłopak__ (_magas fiú_)
- __szybki samolot__ (_gyors repülőgép_)
- __zielony samochód__ (_zöld autó_)
- __przystojny mężczyzna__ (_jóképű férfi_)
- __zielony ogród__ (_zöld kert_)

### Néhány nőnemű példa

- __mała dziewczynka__ (_kicsi lányka_)
- __straszna wiadomość__ (_szörnyű hír_)
- __ładna sekretarka__ (_szép titkárnő_)
- __miła pani__ (_kedves asszony_)
- __sztuka orientalna__ (_keleti művészet_)
- __muzyka klasyczna__ (_klasszikus zene_)
- __rosyjska aktorka__ (_orosz színésznő_)
- __żółta taksówka__ (_sárga taxi_)
- __wysoka gorączka__ (_magas láz_)
- __gorąca czekolada__ (_forrócsoki_)

### Néhány semlegesnemű példa

- __wygodne krzesło__ (_kényelmes szék_)
- __czyste łóźko__ (_tiszta ágy_)
- __głupie dziecko__ (_hülye gyerek_)
- __sensowne zdanie__ (_értelmes mondat_)
- __ogromne lotnisko__ (_óriási repülőtér_)
- __smaczne śniadanie__ (_finom reggeli_)
- __węgierskie miasto__ (_magyar város_)
- __słoneczne mieszkanie__ (_napos lakás_)
- __Morze Czerwone__ (_Vöröstenger_)
- __gorzkie piwo__ (_keserű sör_)

## Többes szám (helyhatározó eset után célszerű tovább olvasni)

A többesszám képzése nem bonyolult, viszont hím-személyes esetben a [helyhatározó esetnél](helyhatarozo_eset-miejscownik.md) leírt hangmódosulásokhoz hasonlóan ilyenkor is változnak a szótövek.
