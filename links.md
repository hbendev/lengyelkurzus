# Linkek

Itt hasznos linkeket találsz.

## Szótárak

1. [Glosbe](https://pl.glosbe.com/)
2. [E-bratanki](http://www.e-bratanki.hu)
3. [Wiktionary](http://www.e-bratanki.hu)
4. [Google translate](https://translate.google.com/)

## Nyelvtani szótárak

1. [Słownik gramatyczny języka polskiego](http://sgjp.pl)

## Egyéb források
1. [Lengyel nyelvtan facebook oldal összefoglalója](https://lengyelvrnk.wordpress.com)
