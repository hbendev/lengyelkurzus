# A történet röviden

Három éve voltam először Lengyelországban egy ismerősömnél. Nagyon tetszett, mert ott minden nagyon hasonlóan néz ki, mint itthon. Teljesen otthon éreztem magam, viszont egy dolgon nem tudtam túltenni magam. Hiába a hasonlóságok, egy kukkot sem értettem abból a nyelvből, amit ott beszélnek. Ez annyira felidegesített, hogy elkeztem még ott beleásni magam a lengyel nyelv rejtelmeibe. Most Budapesten tanulok a Lengyel Nyelvi Intézetben, negyedik féléves vagyok. Nagyon tetszik, ezért elég lelkiismeretesen csinálom. Először csak magamnak kezdtem el csinálni egy összefoglalást, de olyan jól sikerült, hogy a kurzustársak is elkezdték használni. Az egész anyag ingyen elérhető azok számára, akik szeretnék ezt a nyelvet tanulni, de rengeteg időm és energiám megy el vele. Ezt felismerve szükségesnek láttam lehetőséget adni arra, hogy ilyen formában bárki hozzá tudjon járulni, támogatni tudja az anyag elkészítését és karbantartását. Sokat jelent ez nekem, mert nagyon szeretem ezt a nyelvet és szeretném, ha minél többen látnának a "susogás" mögé. Egészen más élmény egy baráttal a saját nyelvén beszélni. :)

Támogatni tehát itt tudsz: https://www.patreon.com/trkadi

# Tartalomjegyzék

_Tipp_: A linkre kattintva mehetsz az egyes tartalmakhoz és a böngésző _vissza_ gombjával (__alt + balranyil__) tudsz visszajönni.

- __1.__ [Tartalomjegyzék](README.md)
- __2.__ Nyelvtan
    - __2.1.__ Fő- és melléknevek (jelzők)
        - __2.1.1.__ [Alanyeset](nyelvtan-grammatyka/esetek-przypadki/alanyeset-mianownik.md)
        - __2.1.2.__ [Eszközhatározó eset](nyelvtan-grammatyka/esetek-przypadki/eszkozhatarozo_eset-narzednik.md)
        - __2.1.3.__ [Tárgyeset](nyelvtan-grammatyka/esetek-przypadki/targyeset-biernik.md)
        - __2.1.4.__ [Birtokos eset](nyelvtan-grammatyka/esetek-przypadki/birtokos_eset-dopelniacz.md)
        - __2.1.5.__ [Helyhatározó eset](nyelvtan-grammatyka/esetek-przypadki/helyhatarozo_eset-miejscownik.md)
        - __2.1.6.__ [Részeseset](nyelvtan-grammatyka/esetek-przypadki/reszeseset-celownik.md)
    - __2.4.__ Számnevek
        - __2.4.1.__ [Tőszámnevek (egy, kettő..)](nyelvtan-grammatyka/szamok-liczby/toszamnevek-liczba_kardynalna.md)
- [Linkek](links.md)
- [Markdown howto](markdown-help/README.md)
